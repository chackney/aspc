const https = require("https");
const exec = require("child_process").exec;

console.log("Starting...");
console.log("====================================================");

// Get URL & AUTH STRING
const URL = process.argv[2];
const AUTH_STRING = process.argv[3];


// Check URL is valid
if (URL.match(/(^https)*(\/projects\/)*/)) {
    console.log("URL Valid!");
    console.log("====================================================");
}
else {
    console.log(URL + " not a valid URL. Please ensure your URL includes the https protocol at the start");
    process.exit(1);
}

// Get protocol, stash instance URL and project code out of the URL provided
const STASH_INSTANCE = URL.substring(8, URL.indexOf("/projects/"));
const PROJECT_CODE = URL.substring(URL.indexOf("/projects/") + 10);

// Output to console the details obtained from the URL
console.log("Stash instance location: " + STASH_INSTANCE);
console.log("Project code: " + PROJECT_CODE);
console.log("====================================================");

https.request(
    {
        "hostname": STASH_INSTANCE,
        "path": "/rest/api/1.0/projects/" + PROJECT_CODE + "/repos",
        "auth": AUTH_STRING,
        "Content-Type": "application/json"
    },
    (response) => {
        let body = "";
        response.on('data', (chunk) => {
            body += chunk;
        });
        response.on("end", () => {
            body = JSON.parse(body);
            console.log("Cloning Repos:");
            for (let repo of body["values"]) {
                console.log(repo["name"]);
                exec("git clone " + repo["cloneUrl"]);
            }
        })
    }
).end();
