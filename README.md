# aspc

_**A**tlassian **S**tash **P**roject **C**lone_

Clone an entire project from an Atlassian Stash instance.

## Why?

Say you had a instance of Atlassian Stash at: `https://stash.harmelodic.com` with a Stash project called `BADGER` that contained 15 repositories that all worked together to build something cool.

It'd be a pain to individually clone down each Stash repo, right? (Yes, it would)

Instead, use ASPC.

## Requirements

- Node.js (v6.4.0)
- Mac or Linux

## Installation & Configuration

- Clone and move anywhere you like. I recommend installing in `/opt/`, so you'd perform:
```
$ cd /opt/ && sudo git clone https://gitlab.com/Harmelodic/aspc.git
```

- (If putting elsewhere) Change the value of `ASPC_HOME` in the `aspc/bin/aspc` file to the location you've moved the files to (defaulted to `/opt/aspc`)

- Add the `aspc/bin` directory to your `$PATH`. Alternatively, create a link with:
```
ln -s /opt/aspc/bin/aspc /usr/local/bin/aspc
```

## Usage

```
$ aspc <STASH_PROJECT_URL>
```
The script will then ask for your Stash Username and Stash Password.

where:

- `<STASH_PROJECT_URL>` is the URL of your Stash project when you navigate to it in the browser.

e.g.

```
$ aspc https://stash.harmelodic.com/projects/BADGER
```

---

_Will only work with HTTPs._

_If you want HTTP, either code it yourself, or start using TLS._

_I recommend the latter._
